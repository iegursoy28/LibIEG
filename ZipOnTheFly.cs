using System;
using System.IO;
using System.IO.Compression;
using System.Net;

namespace LibIEG
{
    public class ZipOnTheFly
    {
        public static void ZipOnTheFlyHttp(HttpListenerContext context, string archivePath)
        {
            string mime = "application/octet-stream";
            context.Response.ContentType = mime;
            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

            ZipOnTheFlyBase(context.Response.OutputStream, System.IO.Directory.GetFiles(archivePath));
        }

        public static void ZipOnTheFlyBase(Stream stream, string[] archiveFiles)
        {
            using (var archive = new ZipArchive(new TrackablePositionStream(stream), ZipArchiveMode.Create, true))
            {
                foreach (var entryItem in archiveFiles)
                {
                    if (!File.Exists(entryItem))
                        continue;

                    using (var fileStream = new FileStream(entryItem, FileMode.Open))
                    {
                        var en = archive.CreateEntry(entryItem, CompressionLevel.NoCompression);
                        using (var o = en.Open())
                        {
                            fileStream.Seek(0, SeekOrigin.Begin);
                            fileStream.CopyTo(o);
                        }
                    }
                }
            }
        }
    }
}