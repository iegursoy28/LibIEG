namespace LibIEG
{
    public class UPNPServiceManager : System.IDisposable
    {
        private static UPNPServiceManager _upnp;

        public static UPNPServiceManager GetInstance()
        {
            if (_upnp == null)
                _upnp = new UPNPServiceManager();

            return _upnp;
        }

        public Rssdp.SsdpRootDevice root_device { get; private set; }
        private Rssdp.SsdpDevicePublisher _Publisher;

        UPNPServiceManager()
        {
            _Publisher = new Rssdp.SsdpDevicePublisher();
        }

        public void AddDevice(Rssdp.SsdpRootDevice device)
        {
            if (device == null)
                return;

            if (root_device != null)
                _Publisher.RemoveDevice(root_device);

            root_device = device;
            _Publisher.AddDevice(root_device);

            // Export device xml
            System.IO.File.WriteAllText(System.IO.Path.Combine(Utils.AppBasePath,"root_device.xml"), root_device.ToDescriptionDocument());
        }

        public void AddService(Rssdp.SsdpService service)
        {
            if (root_device == null || service == null)
                return;

            root_device.AddService(service);
            AddDevice(root_device);
        }

        public void RemoveService(string uuid)
        {
            if (root_device == null || uuid == null)
                return;

            foreach (var item in root_device.Services)
            {
                if (item.Uuid.Equals(uuid))
                    root_device.RemoveService(item);
            }

            AddDevice(root_device);
        }

        public void Stop()
        {
            this.Dispose();
        }

        public void Dispose()
        {
            _Publisher?.Dispose();
        }
    }
}