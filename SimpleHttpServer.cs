using System;
using System.IO;
using System.Net;
using System.Threading;

namespace LibIEG
{
    public class ServerStartParameters
    {
        public string path { get; set; } = System.IO.Directory.GetCurrentDirectory();
        public int port { get; set; } = 7000;
        public object RequestHandler { get; set; }

        public override string ToString()
        {
            return $"URL: http://*:{this.port}\nPath: {this.path}\n";
        }
    }

    public class SimpleHttpServer
    {
        public static SimpleHttpServer Current;

        public static void StartHttpServerOnThread(ServerStartParameters parameters)
        {
            var t = new Thread(StartHttpServerThread);
            t.Start(parameters);
        }

        public static void StartHttpServerOnThread(SimpleHttpServer server)
        {
            var t = new Thread(StartHttpServerThread);
            t.Start(server);
        }

        public static void StopHttpServerOnThread()
        {
            Current.Stop();
            Current = null;
        }

        private static void StartHttpServerThread(object parms)
        {
            if (Current != null)
                StopHttpServerOnThread();

            if (parms is ServerStartParameters)
            {
                var httpParms = parms as ServerStartParameters;
                Current = new SimpleHttpServer(httpParms);
            }
            else if (parms is SimpleHttpServer)
            {
                var server = parms as SimpleHttpServer;
                Current = server;
            }
        }

        private Thread _serverThread;
        private HttpListener _listener;
        public ServerStartParameters _serverParameters { get; private set; }

        public SimpleHttpServer(ServerStartParameters parameters)
        {
            _serverParameters = parameters;
            Initialize();
        }

        public SimpleHttpServer(string path)
        {
            _serverParameters = new ServerStartParameters() { path = path };
            Initialize();
        }

        public SimpleHttpServer(int port)
        {
            _serverParameters = new ServerStartParameters() { port = port };
            Initialize();
        }

        public SimpleHttpServer()
        {
            _serverParameters = new ServerStartParameters();
            Initialize();
        }

        private void Initialize()
        {
            System.Console.WriteLine("SimpleHttpServer Started" + Environment.NewLine + _serverParameters.ToString());
            _serverThread = new Thread(Listen);
            _serverThread.Start();
        }

        public void Stop()
        {
            _listener.Stop();
            _serverThread.Join();
        }

        private void Listen()
        {
            _listener = new HttpListener();
            _listener.Prefixes.Add($"http://*:{_serverParameters.port.ToString()}/");
            _listener.Start();

            while (_listener.IsListening)
            {
                try
                {
                    HttpListenerContext context = _listener.GetContext();
                    context.Response.AddHeader("Access-Control-Allow-Origin", "*");
                    context.Response.AddHeader("Access-Control-Allow-Headers", "*");
                    Process(context);
                }
                catch (Exception) { }
            }
        }

        protected virtual void Process(HttpListenerContext context)
        {
            string filename = context.Request.Url.AbsolutePath.Substring(1);
            Console.WriteLine($"Request file: {filename}");

            string filePath = Path.Combine(_serverParameters.path, filename);
            if (!File.Exists(filePath))
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                context.Response.OutputStream.Close();
                return;
            }

            try
            {
                //Adding permanent http response headers
                string mime;
                context.Response.ContentType = Utils._mimeTypeMappings.TryGetValue(Path.GetExtension(filePath), out mime)
                    ? mime
                    : "application/octet-stream";
                context.Response.AddHeader("Content-Disposition", $"filename=\"{Path.GetFileName(filePath)}\"");
                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
                context.Response.AddHeader("Last-Modified", File.GetLastWriteTime(filePath).ToString("r"));

                Stream input = new FileStream(filePath, FileMode.Open);
                context.Response.ContentLength64 = input.Length;

                byte[] buffer = new byte[1024 * 32];
                int nbytes;
                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                input.Close();

                context.Response.OutputStream.Flush();
                context.Response.StatusCode = (int)HttpStatusCode.OK;
                context.Response.OutputStream.Close();
            }
            catch (Exception e)
            {
                context.Response.StatusDescription = e.ToString();
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.Response.OutputStream.Close();
            }
        }
    }
}