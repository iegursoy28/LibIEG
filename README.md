# LibIEG

Download NuGet [Link](https://www.nuget.org/packages/LibIEG/)

## Sample Usage

```c#
using System;
using System.Net;
using LibIEG;

class Program
{
    static Logger Logger = new Logger("LibIEG Demo") { IsLog = false };
    static void Main(string[] args)
    {
        // Start Http server
        SimpleHttpServer.StartHttpServerOnThread(new ServerStartParameters() { path = Utils.AppBasePath });

        // Create app guid
        var guid = Guid.NewGuid().ToString();

        // Write log to console
        Logger.Debug("Start app with guid: " + guid);

        // Get upnp instance
        var upnpInstance = UPNPServiceManager.GetInstance();

        // Start upnp with device
        upnpInstance.AddDevice(new Rssdp.SsdpRootDevice()
        {
            Uuid = guid,
            FriendlyName = "test",
            Manufacturer = "ieg",
            ModelName = "demo",
            CacheLifetime = TimeSpan.FromSeconds(20),
            Location = new Uri($"http://{Utils.GetLocalIPAddress()}:7000/root_device.xml"),
        });

        // Wait key for exit
        Console.ReadKey();

        // Upnp service stop
        upnpInstance.Stop();

        // Http server stop
        SimpleHttpServer.StopHttpServerOnThread();
    }
}
```
